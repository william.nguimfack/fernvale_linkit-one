#!/bin/sh

# Run openocd with olimex using ftdi library
sudo openocd -f /usr/local/share/openocd/scripts/interface/ftdi/olimex-arm-usb-ocd-h.cfg -f /usr/local/schare/openocd/scripts/board/linkIT.cfg -c "adapter_khz 1" -c "transport select jtag"