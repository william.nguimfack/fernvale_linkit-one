#!/bin/sh

# this file is a simple script to run avatar-pandora
# Set the paths the files on the system.

#PYTHONPATH=/home/william/avatar-pandora/avatar-python QEMU_S2E=/home/william/avatar-pandora/s2e-build/qemu-debug/arm-s2e-softmmu/qemu-system-arm QEMU_ARM=/home/william/avatar-pandora/s2e-build/qemu-release/arm-softmmu/qemu-system-arm UBOOT_BINARY=u-boot python3 test_system.py 


export PYTHONPATH=/home/william/avatar-pandora/avatar-python
export QEMU_ARM=/home/william/avatar-pandora/s2e-build/qemu-debug/arm-s2e-softmmu/qemu-system-arm
export UBOOT_BINARY=u-boot
export QEMU_S2E=/home/william/avatar-pandora/s2e-build/qemu-debug/arm-s2e-softmmu/qemu-system-arm

python3 qemu_integratorcp_uboot.py
