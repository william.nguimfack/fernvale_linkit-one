#!/bin/sh

# Run openocd with jlink.cfg
sudo openocd -f /usr/local/share/openocd/scripts/interface/jlink.cfg -f /usr/local/share/openocd/scripts/board/fernvale.cfg -c "adapter_khz 1" -c "transport select jtag"